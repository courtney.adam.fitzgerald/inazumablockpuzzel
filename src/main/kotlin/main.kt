fun main(args: Array<String>) {

    val r1 = GameState(listOf(
        RotatableBlock("A", 3, listOf("B", "D")),
        RotatableBlock("B", 4, listOf("A", "C")),
        RotatableBlock("C", 2, listOf("B", "D")),
        RotatableBlock("D", 1, listOf("C", "A")),
    ))

    val searcher = Searcher( r1 )

    println(searcher)

    println()

    println(Searcher(GameState(listOf(
        RotatableBlock("A", 1, listOf("B", "D")),
        RotatableBlock("B", 1, listOf("A", "C")),
        RotatableBlock("C", 1, listOf("B", "D")),
        RotatableBlock("D", 1, listOf("C", "A")),
    ))))

    println()

    println(Searcher(GameState(listOf(
        RotatableBlock("A", 4, listOf("B", "D")),
        RotatableBlock("B", 4, listOf("A", "C")),
        RotatableBlock("C", 4, listOf("B", "D")),
        RotatableBlock("D", 4, listOf("C", "A")),
    ))))

    println()

    println(Searcher(GameState(listOf(
        RotatableBlock("A", 4, listOf("C", "D")),
        RotatableBlock("B", 1, listOf("E", "D")),
        RotatableBlock("C", 3, listOf("A", "E")),
        RotatableBlock("D", 1, listOf("B", "A")),
        RotatableBlock("E", 2, listOf("C", "B")),
    ))))

    println()

    println(Searcher(GameState(listOf(
        RotatableBlock("A", 4, listOf("B", "D")),
        RotatableBlock("B", 2, listOf("A", "C")),
        RotatableBlock("C", 4, listOf("B", "D")),
        RotatableBlock("D", 2, listOf("C", "A")),
    ))))

    println()

    println(Searcher(GameState(listOf(
        RotatableBlock("A", 1, listOf("C")),
        RotatableBlock("B", 3, listOf("A", "C")),
        RotatableBlock("C", 2, listOf("A", "E")),
        RotatableBlock("D", 4, listOf("C", "E")),
        RotatableBlock("E", 3, listOf("C")),
    ))))

    println()

    println(Searcher(GameState(listOf(
        FlagBlock("A", 1, listOf("B", "D")),
        FlagBlock("B", 2, listOf("A", "C")),
        FlagBlock("C", 3, listOf("B", "D")),
        FlagBlock("D", 2, listOf("C", "A")),
    ))))

}


data class Node( val action: String, val gameState: GameState, val prior: Node? ){
    override fun toString(): String {
        return "'$action' -> $gameState"
    }
}

class Searcher( initialState: GameState ){

    private val unexplored = mutableListOf(Node("*", initialState, null))
    private val explored = mutableListOf<GameState>()

    private val finalNode: Node by lazy {
        go()
    }

    private fun next(): Node? {
        val current = unexplored.removeFirst()
        return when {
            current.gameState.isFinished() -> {
                current
            }
            explored.contains(current.gameState) -> null
            else -> {
                unexplored.addAll(current.gameState.allNames().map {
                    Node( it, current.gameState.getBlock(it).rotate(), current)
                })
                explored.add(current.gameState)
                null
            }
        }
    }

    private fun go(): Node {
        var temp: Node? = null
        while(  temp == null ){
            temp = next()
        }
        return temp
    }

    override fun toString(): String {
        val temp = mutableListOf<Node>()
        var current: Node? = finalNode
        while ( current != null ) {
            temp.add(current)
            current = current.prior
        }
        return temp.asReversed().joinToString("\n") { it.toString() }
    }


}

class GameState( val blocks: List<Block>){

    init {
        blocks.forEach { it.gameState=this }
    }

    fun getBlock(name: String): Block {
        return blocks.firstOrNull { it.name == name } ?: throw IllegalAccessException()
    }

    fun allNames(): List<String>{
        return blocks.map { it.name }.sorted()
    }

    override fun toString(): String {
        return "[${blocks.sortedBy { it.name }.joinToString { it.toString() }}]"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GameState

        return other.blocks.fold(true ){ acc, block ->
            acc && getBlock(block.name).position == block.position
        }

    }

    fun isFinished(): Boolean {
        val rotateFinished = blocks.filterIsInstance<RotatableBlock>().fold(true){ acc, block ->
            acc && block.position == 4
        }

        val flagFinished = blocks.filterIsInstance<FlagBlock>().fold( true ){ acc, block ->
            acc && block.position == blocks.first { it is FlagBlock }.position
        }

        return rotateFinished && flagFinished
    }

    override fun hashCode(): Int {
        return blocks.hashCode()
    }
}

abstract class Block(val name: String, val connectedBlocks: List<String>) {
    lateinit var gameState: GameState
    abstract val position: Int

    fun rotate(): GameState {
        val changed: List<Block> = connectedBlocks.map { gameState.getBlock(it).autoRotate() }.plus(autoRotate())
        val changedNames = changed.map { it.name }
        val unchanged = gameState.blocks.filterNot { changedNames.contains(it.name) }.map { it.clone() }
        return GameState(changed.plus(unchanged))
    }

    override fun toString(): String {
        return "($name$position)"
    }

    internal abstract fun autoRotate(): Block
    abstract fun clone(): Block
}

class RotatableBlock(name: String, inputPosition: Int, connectedBlocks: List<String>) : Block(name, connectedBlocks) {
    constructor(block: RotatableBlock) : this(block.name, block.position, block.connectedBlocks)

    override val position: Int = if ( inputPosition > 4 ) 1 else inputPosition

    override fun autoRotate():RotatableBlock{
        return RotatableBlock(name, position+1, connectedBlocks)
    }

    override fun clone(): Block {
        return RotatableBlock(this)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RotatableBlock

        if (name != other.name) return false
        if (position != other.position) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + position
        return result
    }

}

class FlagBlock(name: String, inputPosition: Int, connectedBlocks: List<String>) : Block(name, connectedBlocks) {
    constructor(block: FlagBlock) : this(block.name, block.position, block.connectedBlocks)

    override val position: Int = if ( inputPosition > 3) 1 else inputPosition

    override fun autoRotate(): Block {
        return FlagBlock(name, position+1, connectedBlocks)
    }

    override fun clone(): Block {
        return FlagBlock(this)
    }

}
